let form = document.getElementById("form");
let inp = document.getElementById("inp")
let para = document.createElement("p");
form.addEventListener("submit", redirect);

//function for log in, that check if first letter is uppercase and name is minimum 3 characters long, and redirecting to new page
function redirect(event) {
    event.preventDefault();
    let inpValue = document.getElementById("inp").value;

    if (inpValue[0] === inpValue[0].toUpperCase() && inpValue.length >= 3) {
        location.assign("CardsPage.html");
        localStorage.newName = inpValue
        inp.value = " "
    } else {
        document.body.append(para);
        para.innerText =
            "Your name must contain 3 letters and first capital letter";
    }
}